#!/usr/bin/env python3

import re, sys, json, time, collections
from urllib.parse import parse_qs
from http.server import BaseHTTPRequestHandler, HTTPServer


ADDR = "127.0.0.1"
PORT = 8080

BOOK_VALUES = {
    0: "Unknown",
    1: "Reserved + No payment",
    2: "Reserved + Paid (8)",
    3: "Reserved + Paid (10)",
    4: "Delivered + Payment required",
    5: "Delivered + Paid",
    6: "Internal pre-sale disabled (later reservation is possible)",
    7: "No reservation required",
    8: "Undefined",
    9: "Undefined",
    10: "Undefined",
    11: "Team Member + Reserved + No payment",
    12: "Team Member + Reserved + Paid (8)",
    13: "Team Member + Reserved + Paid (10)",
}


def writeLog(msg: str, inline: bool = True) -> None:
    """
    Write a message to the log file
    """

    with open("../data/backend.log", "a", encoding = "UTF-8") as file:
        if inline:
            entry = " - {}".format(msg)
        else:
            entry = "\n{}: {}".format(
                time.strftime("%d.%m.%Y %H:%M:%S", time.localtime()),
                msg
            )

        file.write(entry)
        file.flush()


def toInt(v) -> int:
    """
    Try converting a given value to an integer and fall back to a string
    """

    try:
        return int(v)
    except ValueError as err:
        writeLog("Integer Conversion: ValueError: {}".format(err))
        return str(v)


class RequestHandler(BaseHTTPRequestHandler):
    """
    Backend server process
    """

    requests = [
        "/IsAlive",
        "/GetRankingQuestions",
        "/GetSurveyQuestions",
        "/DoRanking",
        "/DoSurvey",
        "/WhoIs",
        "/ApproveName",
        "/SubmitQuote",
        "/SubmitJourney",
        "/GenerateTable",
        "/GetMyStats"
    ]

    def send(self, status: int, message: str):
        """
        Send a message (response) to the client
        """

        writeLog("[{}]".format(status))

        self.send_response(status)
        self.end_headers()
        self.wfile.write(message.encode("UTF-8"))

    def do_GET(self):
        """
        Handle a GET request
        """

        writeLog("GET {}".format(self.path), False)

        if self.path in self.requests:
            writeLog("Wrong Protocol - [405]")
            self.send_response(405, "Use POST")
            self.end_headers()

        else:
            writeLog("FileNotFound")
            self.send(404, "File Not Found: " + self.path)

    def do_POST(self):
        """
        Handle a POST request
        """

        if self.path == "/IsAlive":
            self.send_response(200)
            self.end_headers()
            self.wfile.write("Alive!".encode("UTF-8"))
            return

        writeLog("POST {}".format(self.path), False)

        length = int(self.headers["Content-Length"])
        data = parse_qs(self.rfile.read(length).decode("utf-8"))

        if self.path == "/GetRankingQuestions":
            returnTooLate(self)

        elif self.path == "/GetSurveyQuestions":
            returnTooLate(self)

        elif self.path == "/DoRanking":
            returnTooLate(self)

        elif self.path == "/DoSurvey":
            returnTooLate(self)

        elif self.path == "/WhoIs":
            whoIs(self, data)

        elif self.path == "/ApproveName":
            approveName(self, data)

        elif self.path == "/SubmitQuote":
            returnTooLate(self)

        elif self.path == "/SubmitJourney":
            returnTooLate(self)

        elif self.path == "/GenerateTable":
            generateTable(self, data)

        elif self.path == "/GetMyStats":
            generateStats(self, data)

        else:
            writeLog("FileNotFound")
            self.send(404, "File Not Found: " + self.path)


def returnTooLate(srv: RequestHandler) -> bool:
    """
    """

    srv.send(410, "You are too late!")
    return False


def sendRankingQuestions(srv: RequestHandler, data: dict) -> bool:
    """
    Read the ranking questions from file and send them to the client
    """

    try:
        uid = toInt(data["id"][0])

    except KeyError as err:
        writeLog("Missing key: {}".format(err))
        srv.send(400, "Missing key: {}".format(err))
        return False

    if not isinstance(uid, int):
        writeLog("UID not an integer")
        srv.send(400, "UID not an integer")
        return False

    with open("../data/people.json", "r", encoding = "UTF-8") as file:
        for person in json.load(file):
            if uid == person["no"]:
                if person["ranking"]:
                    writeLog("Ranking already done!")
                    srv.send(400, "Ranking already done!")
                    return False

    with open("../data/rankings.json", "r", encoding = "UTF-8") as file:
        srv.send(200, json.dumps(json.load(file)))
        return True


def sendSurveyQuestions(srv: RequestHandler, data: dict) -> bool:
    """
    Read the survey questions from file and send them to the client
    """

    try:
        uid = toInt(data["id"][0])

    except KeyError as err:
        writeLog("Missing key: {}".format(err))
        srv.send(400, "Missing key: {}".format(err))
        return False

    if not isinstance(uid, int):
        writeLog("UID not an integer")
        srv.send(400, "UID not an integer")
        return False

    with open("../data/people.json", "r") as file:
        for person in json.load(file):
            if uid == person["no"]:
                if person["survey"]:
                    writeLog("Survey already done!")
                    srv.send(400, "Survey already done!")
                    return False

    with open("../data/survey.json", "r", encoding = "UTF-8") as file:
        srv.send(200, json.dumps(json.load(file)))
        return True


def process(srv: RequestHandler, data: dict, name: str) -> bool:
    """
    Store the supplied data as a user's answer
    """

    with open("../data/answers.json", "r", encoding = "UTF-8") as file:
        foo = json.load(file)

    with open("../data/people.json", "r", encoding = "UTF-8") as file:
        people = json.load(file)

    try:
        result = {
            "time": int(time.mktime(time.localtime())),
            "id": toInt(data["id"][0]),
            "name": data["name"][0]
        }

        del data["id"]
        del data["name"]

        writeLog("User: {} (UID={})".format(result["name"], result["id"]))

        for k in data:
            data[k] = data[k][0]

        for person in people:
            if person["no"] == result["id"] and person[name[:-1]]:
                writeLog("{} already done!".format(name[:-1].title()))
                srv.send(400, "{} already done!".format(name[:-1].title()))
                return False

    except KeyError as err:
        writeLog("Missing key: {}".format(err))
        srv.send(400, "Missing key: {}".format(err))
        return False

    result["query"] = data
    foo[name].append(result)

    with open("../data/answers.json", "w", encoding = "UTF-8") as file:
        json.dump(foo, file, indent = 4, sort_keys = True)

    for p in people:
        if p["no"] == result["id"]:
            p[name[:-1]] = True

    with open("../data/people.json", "w", encoding = "UTF-8") as file:
        json.dump(people, file, indent = 4, sort_keys = True)

    srv.send(200, "OK")
    return True


def whoIs(srv: RequestHandler, data: dict) -> bool:
    """
    Send back a full name or the UID of a user

    If the data contains a field `id` holding a valid UID or list of UIDs,
    the matching username or list of usernames is returned as string.
    If the data contains a field `name` and this is a valid,
    full username, the matching UID (integer) is returned.
    If the data contains a field `name` but this is not a full
    username, then auto completion is tried if there's only one hit.
    """

    def whoIsID(srv: RequestHandler, uid: int) -> bool:
        """
        Send back a full username as string
        """

        if not isinstance(uid, int):
            writeLog("UID not an integer")
            srv.send(400, "UID not an integer")
            return False

        with open("../data/login.json", "r", encoding = "UTF-8") as file:
            for person in json.load(file):
                if uid == person["no"]:
                    srv.send(200, person["name"])
                    return True

        srv.send(400, "Unknown UID")
        return False

    def whoAreIDs(srv: RequestHandler, values: list) -> bool:
        """
        Send back a list of usernames matching the list of UIDs
        """

        with open("../data/login.json", "r", encoding = "UTF-8") as file:
            data = json.load(file)

        results = []
        for uid in values:
            uid = toInt(uid)

            for person in data:
                if uid == person["no"]:
                    results.append(person["name"])
                    break
            else:
                srv.send(400, "Unknown UID: {}".format(uid))
                return False

        srv.send(200, json.dumps(results))
        return True

    def whoIsName(srv: RequestHandler, name: str) -> bool:
        """
        Send back a UID matching the name or auto-complete the username
        """

        with open("../data/login.json", "r", encoding = "UTF-8") as file:
            hits = []
            for person in json.load(file):
                if name.lower() in person["name"].lower():
                    hits.append(person)
                if name == person["name"]:
                    writeLog("Found: {}".format(person["no"]))
                    srv.send(200, str(person["no"]))
                    return True
                elif name.title() == person["name"]:
                    writeLog("Found: {}".format(person["name"]))
                    srv.send(200, person["name"])
                    return True

        if len(hits) == 1:
            writeLog("Completed: {}".format(hits[0]["name"]))
            srv.send(200, hits[0]["name"])
            return True

        writeLog("Unknown or ambiguous name")
        srv.send(400, "Unknown or ambiguous name")
        return False

    if "name" in data.keys() and "id" in data.keys():
        writeLog("Multiple unique keys")
        srv.send(400, "Multiple unique keys")
        return False

    elif "name" in data.keys():
        name = data["name"][0]
        writeLog("Name: {}".format(name))
        return whoIsName(srv, name)

    elif "id" in data.keys():
        value = data["id"][0]
        if re.match("^[0-9]+$", value):
            uid = toInt(value)
            writeLog("UID: {}".format(uid))
            return whoIsID(srv, uid)

        elif re.match("^\[(([0-9]+),?)+\]$", value):
            try:
                values = list(set(json.loads(value)))
            except json.decoder.JSONDecodeError as err:
                writeLog("Broken JSON: {}".format(err))
                srv.send(400, "Broken JSON: {}".format(err))
                return False

            writeLog("Checking {} UIDs".format(len(values)))
            return whoAreIDs(srv, values)

        else:
            writeLog("Invalid request: {}".format(value))
            srv.send(400, "Invalid request: {}".format(value))
            return False

    else:
        writeLog("Missing key: 'name' or 'id'")
        srv.send(400, "Missing key: 'name' or 'id'")
        return False


def approveName(srv: RequestHandler, data: dict) -> bool:
    """
    Ensure that a given name is a valid username
    """

    try:
        name = data["name"][0].title()
    except KeyError as err:
        writeLog("Missing key: {}".format(err))
        srv.send(400, "Missing key: {}".format(err))
        return False

    writeLog("Name: {}".format(name))

    with open("../data/people.json", "r", encoding = "UTF-8") as file:
        hits = []
        people = json.load(file)
        for person in people:
            if name in person["name"]:
                hits.append(person)

    srv.send(200, json.dumps({
        "name": name,
        "person": None if len(hits) != 1 else hits[0],
        "approved": len(hits) == 1,
        "hits": len(hits)
    }))

    if len(hits) == 1:
        writeLog("Approved: True - UID: {}".format(hits[0]["no"]))

        for person in people:
            if person["no"] == hits[0]["no"]:
                person["accesses"] += 1
                person["visits"].append(time.mktime(time.localtime()))

        with open("../data/people.json", "w", encoding = "UTF-8") as file:
            json.dump(people, file, indent = 4, sort_keys = True)

    else:
        writeLog("Approved: False")

    return len(hits) == 1


def submitQuote(srv: RequestHandler, data: dict) -> bool:
    """
    Store the supplied quote text in the storage file
    """

    try:
        uid = toInt(data["id"][0])
        entry = {
            "name": data["name"][0],
            "id": uid,
            "time": int(time.mktime(time.localtime())),
            "quote": data["quote"][0]
        }

    except KeyError as err:
        writeLog("Missing key: {}".format(err))
        srv.send(400, "Missing key: {}".format(err))
        return False

    writeLog("Name: {}".format(entry["name"]))
    writeLog("Quote Length: {}".format(len(entry["quote"])))

    with open("../data/quotes.json", "r", encoding = "UTF-8") as file:
        quotes = json.load(file)

    with open("../data/quotes.json", "w", encoding = "UTF-8") as file:
        quotes.append(entry)
        json.dump(quotes, file, indent = 4, sort_keys = True)

    with open("../data/people.json", "r", encoding = "UTF-8") as file:
        people = json.load(file)

    with open("../data/people.json", "w", encoding = "UTF-8") as file:
        for person in people:
            if person["no"] == uid:
                person["quotes"] += 1
        json.dump(people, file, indent = 4, sort_keys = True)

    srv.send(200, "Danke für das Abschicken dieses Zitats!")
    return True


def submitJourney(srv: RequestHandler, data: dict) -> bool:
    """
    Save the supplied story about the journey
    """

    try:
        entry = {
            "name": data["name"][0],
            "id": toInt(data["id"][0]),
            "time": int(time.mktime(time.localtime())),
            "story": data["story"][0]
        }

        key = data["destination"][0].lower()
        if key not in ["london", "paris", "segeln", "sorrent"]:
            writeLog("Wrong key: {}".format(key))
            srv.send(400, "Wrong key: {}".format(key))
            return False

    except KeyError as err:
        writeLog("Missing key: {}".format(err))
        srv.send(400, "Missing key: {}".format(err))
        return False

    writeLog("Name: {}".format(entry["name"]))
    writeLog("Destination: {}".format(key))
    writeLog("Story Length: {}".format(len(entry["story"])))

    with open("../data/journeys.json", "r", encoding = "UTF-8") as file:
        stories = json.load(file)

    with open("../data/journeys.json", "w", encoding = "UTF-8") as file:
        stories[key].append(entry)
        json.dump(stories, file, indent = 4, sort_keys = True)

    srv.send(200, "Danke für das Abschicken dieser Story!")
    return True


def generateTable(srv: RequestHandler, data: dict) -> bool:
    """
    Generate and send back the individual HTML profile table (using the UID)
    """

    try:
        uid = toInt(data["id"][0])
    except KeyError as err:
        writeLog("Missing key: {}".format(err))
        srv.send(400, "Missing key: {}".format(err))
        return False

    writeLog("UID: {}".format(uid))

    with open("../data/people.json", "r", encoding = "UTF-8") as file:
        people = json.load(file)

    for person in people:
        if person["no"] == uid:
            with open(
                    "../html/profile.raw.html",
                    "r",
                    encoding = "UTF-8"
                ) as file:

                try:
                    book_value = BOOK_VALUES[person["book"]]
                    if person["book"] == 0:
                        book_value += "<br/>Please come and talk to us!"
                except KeyError as err:
                    writeLog("KeyError: {} (using person[book])".format(err))
                    book_value = "Internal Server Error"

                result = file.read().format(
                    **person,
                    logins = "<br/>".join([
                        time.strftime(
                            "%d.%m.%Y %H:%M:%S",
                            time.localtime(i)
                        ) for i in person["visits"][-5:]
                    ]),
                    book_value = book_value
                )

                writeLog("Generated {} bytes of output".format(len(result)))
                srv.send(200, result)
                return True

    writeLog("Unknown UID")
    srv.send(400, "Unknown UID: {}".format(uid))
    return False


def generateStats(srv: RequestHandler, data: dict) -> bool:
    """
    Generate the table of personal ranking stats and send it to the client
    """

    try:
        uid = toInt(data["id"][0])
    except KeyError as err:
        writeLog("Missing key: {}".format(err))
        srv.send(400, "Missing key: {}".format(err))
        return False

    writeLog("UID: {}".format(uid))

    with open("../data/people.json", "r", encoding = "UTF-8") as file:
        people = json.load(file)

    with open("../data/answers.json", "r", encoding = "UTF-8") as file:
        answers = json.load(file)

    with open("../data/rankings.json", "r", encoding = "UTF-8") as file:
        rankings = json.load(file)["list"]

    name = ""
    awards = collections.OrderedDict()
    submits = 0
    categories = {}

    for person in people:
        if person["ranking"]:
            submits += 1
        if person["no"] == uid:
            name = person["name"]

    if name == "":
        writeLog("Unknown UID")
        srv.send(400, "Unknown UID: {}".format(uid))
        return False

    for q in rankings:
        if q["category"] == "Sch\u00fcler":
            categories[q["id"]] = q["title"]
        else:
            categories[q["id"]] = "FORBIDDEN"

    for obj in answers["rankings"]:
        keys = list(obj["query"].keys())
        keys.sort()
        for key in keys:
            if obj["query"][key] == name:
                if key not in awards.keys():
                    awards[key] = 1
                else:
                    awards[key] += 1

    body = ""
    keys = list(awards.keys())
    keys.sort()
    for each in keys:
        if categories[each[:-3]] == "FORBIDDEN":
            writeLog("User nominated {}x for category {}!".format(
                awards[each], each
            ))
            continue

        body += "<tr><td>{k}</td><td>{i}</td><td>{v}</td></tr>\n".format(
            k = "{} ({})".format(categories[each[:-3]], each[-2]),
            i = each,
            v = '<b>{}</b>&ensp;&Rightarrow;&ensp;~{:.1f}%'.format(
                awards[each], 100 * awards[each] / submits
            )
        )

    with open("../html/stats.raw.html", "r", encoding = "UTF-8") as file:
        result = file.read().format(
            tbody = body,
            num = submits
        )

        writeLog("Generated {} bytes of output".format(len(result)))
        srv.send(200, result)
        return True

    writeLog("Unknown error!")
    srv.send(500, "Unkown error!")
    return False


if __name__ == "__main__":
    httpd = HTTPServer((ADDR, PORT), RequestHandler)

    writeLog("Backend process starting on {}:{} ...".format(ADDR, PORT), False)
    print("Quit with Ctrl+C!")

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        writeLog("Backend process stopped by KeyboardInterrupt!\n", False)
        sys.exit(0)
    except:
        import traceback
        writeLog("Exception!\n" + traceback.format_exc(), False)
