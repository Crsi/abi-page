// Feature JS Script

/*
*
* SETUP THE PROFILE TABLE
*
*/

function setupProfileTable() {
	$.ajax({
		cache: false,
		data: {
			"id": user.person.no
		},
		error: function(xhr, status, error) {
			handleError(
				"#alert-profile",
				xhr,
				"Es ist ein Fehler aufgetreten!",
				"alert-danger",
				allAlertClasses
			);
		},
		success: function(result, status, xhr) {
			$("#profile table.auth-required").html(result);
		},
		type: "POST",
		url: "/s/GenerateTable"
	});
}

/*
*
* QUOTE FEATURES
*
*/

function sendQuote() {
	if (!user.approved) {
		$("#username-modal").modal();
	}

	else {
		var quote = $("#quote-input-box")[0].value;

		if (user.quotes.indexOf(quote) == -1) {
			$.ajax({
				cache: false,
				data: {
					"name": user.username,
					"id": user.person.no,
					"quote": quote
				},
				error: function(xhr, status, error) {
					handleError(
						"#alert-quotes",
						xhr,
						"Es ist ein Fehler aufgetreten!",
						"alert-danger",
						allAlertClasses
					);
				},
				success: function(result, status, xhr) {
					user.quotes.push(quote);
					setupAlert(
						"#alert-quotes",
						"Danke!",
						"Vielen Dank. Das Zitat wurde gespeichert.",
						"alert-success",
						allAlertClasses
					);
				},
				type: "POST",
				url: "/s/SubmitQuote"
			});
		}

		else {
			setupAlert(
				"#alert-quotes",
				"Danke!",
				"Anscheinend hast du dieses Zitat bereits gespeichert.",
				"alert-info",
				allAlertClasses
			);
		}
	}
}

/*
*
* JOURNEY FEATURES
*
*/

function uploadJourney() {
	if (!user.approved) {
		$("#username-modal").modal();
	}

	else {
		var story = $("#journey-input-box")[0].value;
		if (user.journey.stories.indexOf(story) != -1) {
			setupAlert(
				"#alert-journey",
				"Danke!",
				"Du scheinst diese Story bereits hochgeladen zu haben!",
				"alert-info",
				allAlertClasses
			);
			return;
		}

		$.ajax({
			cache: false,
			data: {
				id: user.person.no,
				name: user.username,
				destination: user.journey.location,
				story: story
			},
			error: function(xhr, status, error) {
				handleError(
					"#alert-journey",
					xhr,
					"Ein Fehler ist aufgetreten!",
					"alert-danger",
					allAlertClasses
				);
			},
			success: function(result, status, xhr) {
				user.journey.stories.push(story);
				handleError(
					"#alert-journey",
					xhr,
					"Danke für diesen Upload!",
					"alert-success",
					allAlertClasses
				);
			},
			type: "POST",
			url: "/s/SubmitJourney"
		});
	}
}

/*
*
* RANKING FEATURES
*
*/

function setRankingProgress(value) {
	$("#ranking-progress")[0].style.width = value;
	$("#ranking-progress")[0].innerText = value;
}

function fillRanking(question) {
	$("#ranking-modal div.modal-body h5 span b").text(question["category"]);
	$("#ranking-modal mark strong.r-category").text(question["title"]);
	$("#ranking-heading-category").text(question["category"]);
	$("#ranking-answer-m")[0].placeholder = question["hint-m"];
	$("#ranking-answer-w")[0].placeholder = question["hint-w"];
	$("#ranking-answer-m")[0].classList.remove("is-valid");
	$("#ranking-answer-w")[0].classList.remove("is-valid");
	$("#ranking-answer-m")[0].classList.remove("is-invalid");
	$("#ranking-answer-w")[0].classList.remove("is-invalid");
	$("#ranking-answer-m")[0].value = "";
	$("#ranking-answer-w")[0].value = "";
	user.active.ranking.id = question["id"];
}

function approveRankingName(e) {
	if (e.target.value === "") {
		return;
	}
	if (user.ranking.change <= user.active.ranking.index) {
		return;
	}

	$.ajax({
		cache: false,
		data: {
			name: e.target.value
		},
		error: function(xhr, status, error) {
			e.target.classList.remove("is-valid");
			e.target.classList.remove("is-invalid");

			e.target.classList.add("is-invalid");
		},
		success: function(result, status, xhr) {
			e.target.classList.remove("is-valid");
			e.target.classList.remove("is-invalid");

			if (result !== "" && !isNaN(result)) {
				e.target.classList.add("is-valid");
			}

			else if (result !== "" && isNaN(result)) {
				e.target.value = result;
				e.target.classList.add("is-valid");
			}

			else {
				console.error(xhr);
			}
		},
		type: "POST",
		url: "/s/WhoIs"
	});
}

function doRanking() {
	user.active.ranking.index += 1;

	if (user.active.ranking.index > user.ranking.total) {
		return;
	}

	let m = $("#ranking-answer-m")[0].value;
	let w = $("#ranking-answer-w")[0].value;
	let mInvalid = $("#ranking-answer-m")[0].classList.contains("is-invalid");
	let wInvalid = $("#ranking-answer-w")[0].classList.contains("is-invalid");

	if ((mInvalid && m !== "") || (wInvalid && w !== "")) {
		user.active.ranking.index -= 1;
		return;
	}

	if (m === "") {
		m = "UNSET";
	}
	if (w === "") {
		w = "UNSET";
	}

	setRankingProgress(
		Math.round(
			user.active.ranking.index / user.ranking.total * 100
		) + "%"
	);

	user.answers.ranking[user.active.ranking.id] = {
		"m": m,
		"w": w
	};

	if (user.active.ranking.index <= user.ranking.total - 1) {
		if (user.active.ranking.index + 1 == user.ranking.change) {
			$("#ranking-modal-next")[0].innerText = "Weiter zu den Lehrerfragen";
		}
		if (user.active.ranking.index == user.ranking.change) {
			$("#ranking-modal-next")[0].innerText = "Weiter";
			$("#ranking-modal .modal-body p.text-success").remove();
		}
		if (user.active.ranking.index == user.ranking.total - 1) {
			$("#ranking-modal-next")[0].innerText = " Finish & Upload! ";
		}

		fillRanking(user.ranking.list[user.active.ranking.index]);
	}

	else if (user.active.ranking.index == user.ranking.total) {
		$("#ranking-modal p.ranking-response").html(
			"Einen Moment Geduld bitte ..."
		);

		$.ajax({
			cache: false,
			data: Object.assign(
				{
					id: user.person.no,
					name: user.username
				},
				user.answers.ranking
			),
			error: function(xhr, status, error) {
				$("#ranking-modal p.ranking-response").html(
					"<b>Fehler!</b> Bitte versuche es erneut oder kontaktiere den Admin..."
				);
				console.error(xhr);
				user.active.ranking.index -= 1;
			},
			success: function(result, status, xhr) {
				$("#ranking-modal p.ranking-response").html(
					"Alles erledigt! Danke!"
				);
			},
			type: "POST",
			url: "/s/DoRanking"
		});
	}
}

function startRanking() {
	if (user.approved && user.ranking != null) {
		if (user.active.ranking.index !== -1) {
			user.active.ranking.index -= 1;
		}

		doRanking();
		$("#ranking-modal").modal();
	}

	else if (user.approved) {
		$.ajax({
			cache: false,
			data: {
				id: user.person.no
			},
			error: function(xhr, status, error) {
				handleError(
					"#alert-survey",
					xhr,
					"Es ist ein Fehler aufgetreten!",
					"alert-danger",
					allAlertClasses
				);
			},
			success: function(result, status, xhr) {
				user.ranking = JSON.parse(result);
				doRanking();
				$("#ranking-modal").modal();
			},
			type: "POST",
			url: "/s/GetRankingQuestions"
		});
	}

	else {
		$("#username-modal").modal();
	}
}

/*
*
* SURVEY FEATURES
*
*/

function setSurveyProgress(value) {
	$("#survey-progress")[0].style.width = value;
	$("#survey-progress")[0].innerText = value;
}

function fillSurvey(question) {
	$("#survey-modal mark strong.question").text(question["title"]);
	$("#survey-modal-answer")[0].placeholder = question["hint"];
	$("#survey-modal-answer")[0].value = "";
	user.active.survey.id = question["id"];
}

function doSurvey() {
	user.active.survey.index += 1;

	if (user.active.survey.index > user.survey.total) {
		return;
	}

	setSurveyProgress(
		Math.round(
			user.active.survey.index / user.survey.total * 100
		) + "%"
	);

	var value = $("#survey-modal-answer")[0].value;
	if (value === "") {
		value = "UNSET";
	}

	user.answers.survey[user.active.survey.id] = value;

	if (user.active.survey.index <= user.survey.total - 1) {
		fillSurvey(user.survey.list[user.active.survey.index]);

		if (user.active.survey.index == user.survey.total - 1) {
			$("#survey-modal-next")[0].innerText = " Finish & Upload! ";
		}
	}

	else if (user.active.survey.index == user.survey.total) {
		$("#survey-modal p.survey-response").html(
			"Einen Moment Geduld bitte ..."
		);

		$.ajax({
			cache: false,
			data: Object.assign(
				{
					id: user.person.no,
					name: user.username
				},
				user.answers.survey
			),
			error: function(xhr, status, error) {
				$("#survey-modal p.survey-response").html(
					"<b>Fehler!</b> Bitte versuche es erneut oder kontaktiere den Admin..."
				);
				console.error(xhr);
				user.active.survey.index -= 1;
			},
			success: function(result, status, xhr) {
				$("#survey-modal p.survey-response").html(
					"Alles erledigt! Danke!"
				);
			},
			type: "POST",
			url: "/s/DoSurvey"
		});
	}
}

function startSurvey() {
	if (user.approved && user.survey != null) {
		if (user.active.survey.index !== -1) {
			user.active.survey.index -= 1;
		}

		doSurvey();
		$("#survey-modal").modal();
	}

	else if (user.approved) {
		$.ajax({
			cache: false,
			data: {
				id: user.person.no
			},
			error: function(xhr, status, error) {
				handleError(
					"#alert-survey",
					xhr,
					"Es ist ein Fehler aufgetreten!",
					"alert-danger",
					allAlertClasses
				);
			},
			success: function(result, status, xhr) {
				user.survey = JSON.parse(result);
				$("#survey-modal").modal();
				doSurvey();
			},
			type: "POST",
			url: "/s/GetSurveyQuestions"
		});
	}

	else {
		$("#username-modal").modal();
	}
}
