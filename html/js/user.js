// User JS Script

var user = {
	input: "",
	username: "",
	approved: false,
	ranking: null,
	survey: null,
	answers: {
		ranking: {},
		survey: {}
	},
	active: {
		ranking: {
			index: -1,
			id: ""
		},
		survey: {
			index: -1,
			id: ""
		}
	},
	quotes: [],
	journey: {
		location: null,
		stories: []
	},
	person: null,
	hits: -1
}

function fixMissingNames(values, selector) {
	$.ajax({
		cache: false,
		data: {
			"id": JSON.stringify(values)
		},
		error: function(xhr, status, error) {
			handleError(
				"#alert-profile",
				xhr,
				"Es ist ein Fehler aufgetreten!",
				"alert-danger",
				allAlertClasses
			);
		},
		success: function(result, status, xhr) {
			$(selector).text(JSON.parse(result).join(", "));
		},
		type: "POST",
		url: "/s/WhoIs"
	});
}

function setupApprovedUI() {
	$("#username-input-submit").addClass("disabled");

	if (!user.person.ranking) {
		$("#ranking-input-submit").show();
	} else {
		$("#ranking-stats").load(
			"/s/GetMyStats",
			{
				id: user.person.no
			},
			function(result, status, xhr) {
				if (status == "error") {
					handleError(
						"#alert-survey",
						xhr,
						"Es ist ein Fehler aufgetreten!",
						"alert-danger",
						allAlertClasses
					)
				}
			}
		);
	}

	$("#survey-input-submit").removeClass("btn-outline-primary");
	$("#survey-input-submit").addClass("btn-outline-success");
	$("#survey-input-submit")[0].innerText = "Umfrage ausfüllen!";

	$("#quote-input-box").show();
	$("#quote-input-submit").removeClass("btn-outline-primary");
	$("#quote-input-submit").addClass("btn-outline-success");
	$("#quote-input-submit")[0].innerText = "Zitat hochladen!";

	$("#journey-input-box").show();
	$("#journey-input-group").show();
	$("#journey-input-submit").hide();

	$(".auth-required").show();
	$(".auth-unnecessary").hide();

	if (user.person.survey) {
		$(".survey-done").show();
		$("#survey-input-submit").hide();
	} else {
		$(".survey-done").hide();
	}

	if (user.person.survey && user.person.ranking) {
		$(".both-enabled").hide();
	}

	$("#profile h5.auth-required span.name").text(user.username);

	if (user.person.tasks.friends.length !== 0) {
		fixMissingNames(user.person.tasks.friends, "#friends-friends");
	}
	if (user.person.tasks.todo.length !== 0) {
		fixMissingNames(user.person.tasks.todo, "#friends-task");
	}

	setupProfileTable();
}

function approveName(result) {
	result = JSON.parse(result);

	if (!result.approved) {
		setupAlert(
			"#alert-username",
			"Fehler!",
			"Dein Name konnte nicht bestätigt werden! "
			+ "Bist du sicher, dass du deinen Namen korrekt "
			+ "geschrieben hast? Denn die Suche ergab nicht "
			+ "einen sondern " + result.hits + " Treffer!",
			"alert-danger",
			allAlertClasses
		);

	} else {
		user.approved = true;
		user.hits = result.hits;
		user.person = result.person;
		user.username = result.person.name;

		var personal = "vorhanden!";
		var friends = "vorhanden!";
		var survey = "vorhanden!";

		if (!result.person.sites.personal) {
			personal = "fehlt!";
		}
		if (!result.person.sites.friends) {
			friends = "fehlt!";
		}
		if (!result.person.survey) {
			survey = "fehlt!";
		}

		setupAlert(
			"#alert-username",
			"Okay!",
			"Dein Name konnte bestätigt werden. Hallo, " + user.username
			+ "! Überprüfe deine letzten Logins in deinem Profil.",
			"alert-success",
			allAlertClasses
		);
		
		setupApprovedUI();
	}
}

function checkName() {
	if ($("#username-input-submit").hasClass("disabled")) {
		$("#username-modal").modal("hide");
		return;
	}

	user.input = $("#username-input-name")[0].value;

	$.ajax({
		cache: false,
		data: {
			"name": user.input
		},
		error: function(xhr, status, error) {
			handleError(
				"#alert-username",
				xhr,
				"Es ist ein Fehler aufgetreten!",
				"alert-danger",
				allAlertClasses
			);
		},
		success: function(result, status, xhr) {
			approveName(result);
		},
		type: "POST",
		url: "/s/ApproveName"
	});
}
