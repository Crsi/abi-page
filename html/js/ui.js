// User Interface JS Script

$(document).ready(function() {
	$("body").fadeIn("slow");
	$(".auth-required").hide();

	//$("#alert-setup").hide();
	$("#alert-setup div .close").click(function() {
		$("#alert-setup").fadeOut();
	});

	$("#alert-main").hide();
	$("#alert-main div .close").click(function() {
		$("#alert-main").fadeOut();
	});
	$("#alert-profile").hide();
	$("#alert-profile div .close").click(function() {
		$("#alert-quotes").fadeOut();
	});
	$("#alert-quotes").hide();
	$("#alert-quotes div .close").click(function() {
		$("#alert-quotes").fadeOut();
	});
	$("#alert-journey").hide();
	$("#alert-journey div .close").click(function() {
		$("#alert-journey").fadeOut();
	});
	$("#alert-survey").hide();
	$("#alert-survey div .close").click(function() {
		$("#alert-survey").fadeOut();
	});
	$("#alert-username").hide();
	$("#alert-username div .close").click(function() {
		$("#alert-username").fadeOut();
	});

	$(".login-button").click(function() {
		$("#username-modal").modal();
	})

	$("#quote-input-submit").click(sendQuote);
	$("#ranking-input-submit").click(startRanking);
	$("#survey-input-submit").click(startSurvey);
	$("#journey-input-submit").click(uploadJourney);
	$("#username-input-submit").click(checkName);

	$("#ranking-input-submit").hide();

	$("#username-input-name").on("keydown", function(event) {
		if (event.which == 13 || event.keyCode == 13 || event.key == "Enter") {
			event.preventDefault();
			checkName();
		}
	});

	$("#survey-modal-next").click(doSurvey);
	$("#survey-modal-answer").on("keydown", function(event) {
		if (event.which == 13 || event.keyCode == 13 || event.key == "Enter") {
			event.preventDefault();
			doSurvey();
		}
	});

	$("#ranking-modal-next").click(doRanking);
	/*
		$("#ranking-answer-m").on("keydown", function(event) {
			if (event.which == 13 || event.keyCode == 13 || event.key == "Enter") {
				event.preventDefault();
				doRanking();
			}
		});
	*/

	$("#quote-input-box").hide();
	$("#journey-input-box").hide();
	$("#journey-input-group").hide();

	$("#journey-submit-london").click(function() {
		user.journey.location = "London";
		uploadJourney();
	});
	$("#journey-submit-paris").click(function() {
		user.journey.location = "Paris";
		uploadJourney();
	});
	$("#journey-submit-segeln").click(function() {
		user.journey.location = "Segeln";
		uploadJourney();
	});
	$("#journey-submit-sorrent").click(function() {
		user.journey.location = "Sorrent";
		uploadJourney();
	});

	setSurveyProgress("0%");
	setRankingProgress("0%");

	$("#ranking-answer-m, #ranking-answer-w").blur(approveRankingName);

	$("a").each(function(index, element) {
		element.target = "_blank";
		element.rel = "noopener noreferrer";
	});
})
