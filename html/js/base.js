// Base JS Script

var allAlertClasses = [
	"alert-danger",
	"alert-dark",
	"alert-info",
	"alert-light",
	"alert-primary",
	"alert-secondary",
	"alert-success",
	"alert-warning"
];

function setupAlert(selector, heading, message, classAdd, classDel) {
	$(selector).show();
	$(selector + " .alert").removeClass(classDel).addClass(classAdd);
	$(selector + " div .alert-heading").text(heading);
	$(selector + " div .alert-body").html(message);
}

function handleError(selector, xhr, messageDefault, classAdd, classDel) {
	var heading = xhr.status + " " + xhr.statusText + "!";
	var message = messageDefault;
	if (xhr.responseText != "") {
		message = xhr.responseText;
	}
	setupAlert(selector, heading, message, classAdd, classDel);
}

function setupGenericErrorAlert(selector) {
	setupAlert(
		selector,
		"Error!",
		"Something went wrong! Please contact the administrator."
		+ " You may have to reload the page to fix the problem.",
		"alert-danger",
		allAlertClasses
	);
}
