<?php

// Attention:
// This file will be imported by the other scripts!
// Do not put any direct output rules (e.g. echo) here!

// Configuration:
// In order to know the location of the running backend service,
// you need to adjust the following line (without trailing slash)!
// Default value: "http://localhost:8080"
define("SERVICE", "http://localhost:8080");

// Configuration:
// In order to write log files, you have to give a valid filename
// where access logs should be written to. Note that PHP
// needs the permission to write to that file! Set this to an
// empty string to disable logging!
define("LOG_FILE", "/var/www/data/access.log");

// Deny access if the lib was called directly
// in case it's placed in the web root
if (basename(__FILE__) == basename($_SERVER["SCRIPT_FILENAME"])) {
	http_response_code(401);
	exit();
}

/**
* Ensure that a script is only called via POST and not otherwise
* @return null
*/
function ensurePOST() {
	if ($_SERVER["REQUEST_METHOD"] != "POST") {
		http_response_code(405);
		header("Allow: POST");
		exit("Only POST allowed!");
	}
}

/**
* Send a GET or POST requst using cURL
* @param string $url URI to request
* @param bool $post indicate if POST should be used (false: GET)
* @param array $vars values to send
* @param array $options additional options passed to cURL
* @return array [result, code]
*/
function doRequest(
	string $url,
	bool $post,
	array $vars = NULL,
	array $options = array()
) {
	$defaults = array(
		CURLOPT_HEADER => 0,
		CURLOPT_FRESH_CONNECT => 1,
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_FORBID_REUSE => 1,
		CURLOPT_TIMEOUT => 4
	);

	if ($post) {
		$defaults = $defaults + array(
			CURLOPT_URL => $url,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => http_build_query($vars)
		);
	} else if (count($vars) > 0) {
		$defaults = $defaults + array(
			CURLOPT_URL => $url . "?" . http_build_query($vars)
		);
	} else {
		$defaults = $defaults + array(
			CURLOPT_URL => $url
		);
	}

	$ch = curl_init();
	curl_setopt_array($ch, ($options + $defaults));

	if (!$result = curl_exec($ch)) {
		trigger_error(curl_error($ch));
	}

	$httpcode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

	curl_close($ch);

	return [
		"result" => $result,
		"code" => $httpcode
	];
}

/**
* Forward a request to the backend service and handle the response with `echo`
* @param string $url URI to request
* @param bool $post indicate if POST should be used (false: GET)
* @param array $vars values to send
* @param array $options additional options passed to cURL
* @return null
*/
function forwardRequest(
	string $url,
	bool $post,
	array $vars = NULL,
	array $options = array()
) {
	$response = doRequest(
		SERVICE . $url,
		$post,
		$vars,
		$options
	);

	if ($response["code"] == 0) {
		http_response_code(500);
		echo "Service unavailable! Please contact the administrator.";
	} else {
		http_response_code($response["code"]);
		echo $response["result"];
	}
}

/**
* Creates a log entry and writes it at the end of the log file
* @param string $name indicate the script name
* @return null
*/
function writeLog(string $name = "") {
	if (LOG_FILE == "") {
		return;
	}

	if ($name == "") {
		$name = $_SERVER["SCRIPT_NAME"];
	}

	$ip = $_SERVER["HTTP_X_REAL_IP"];
	if ($ip == NULL) {
		$ip = $_SERVER["REMOTE_ADDR"];
		if ($ip == NULL) {
			$ip = "UNKNOWN ADDR";
		}
	}

	$client = $_SERVER["HTTP_USER_AGENT"];
	if ($client == NULL) {
		$client = "UNKNOWN AGENT";
	}

	$d = sprintf(
		"%s: %s %s from %s (%s)\n",
		date("d.m.Y H:i:s"),
		$_SERVER["REQUEST_METHOD"],
		$name,
		$ip,
		$client
	);

	$h = fopen(LOG_FILE, "a");
	fwrite($h, $d);
	fclose($h);
}

?>
