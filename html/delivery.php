<?php

// Require the library and the configuration
require_once("lib.php");

// Write a log message that this script has been called
writeLog($_SERVER["REQUEST_URI"]);

// Deny access if the script was called directly
if ($_SERVER["REQUEST_URI"] == $_SERVER["SCRIPT_NAME"]) {
	http_response_code(401);
	exit();
}

// Get the request URI that should be forwarded
if (substr($_SERVER["REQUEST_URI"], 0, 3) === "/s/") {
	$request = substr($_SERVER["REQUEST_URI"], 2);
} else {
	http_response_code(400);
	exit();
}

// Check if the script was called using POST
ensurePOST();

// Forward the request to the backend service
forwardRequest(
	$request,
	true,
	$_REQUEST
);

?>
